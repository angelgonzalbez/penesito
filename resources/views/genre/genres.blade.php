@extends('layouts.master')
@section('content')
<div class="row">
        <table class="table">
            <thead>
                <tr>
                    <th><h3>Nom</h3></th>
                    <th style="text-align: center"><h3>Editar</h3></th>
                    <th style="text-align: center"><h3>Eliminar</h3></th>
                </tr>
            </thead>
            <tbody>
                @foreach( $genres as $genre )
                    <tr>
                        <td><h3>{{$genre->title}}</h3></td>
                        <td style="text-align: center"><a href="\genre\edit\{{$genre->id}}" class="btn btn-success">Editar gènere<i class="fas fa-edit"></i></a></td>
                        <td style="text-align: center"><a href='\genre\delete\{{$genre->id}}' class="btn btn-danger">Eliminar gènere</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
</div>
@stop
