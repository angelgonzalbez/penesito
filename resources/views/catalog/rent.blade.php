@extends('layouts.master')
@section('content')
<div class="row">
        <table class="table">
            <thead>
                <tr>
                    <th><h3>Película</h3></th>
                    <th style="text-align: center"><h3>Data alquiler</h3></th>
                    <th style="text-align: center"><h3>Data tornada</h3></th>
                    <th style="text-align: center"><h3>Usuari</h3></th>
                </tr>
            </thead>
            <tbody>
                @foreach( $alquileres as $alquiler )
                    <tr>
                        <td><img src="{{$alquiler->pelicula->poster}}" style="height:200px"/></td>
                        <td style="text-align: center">{{ $alquiler->dateRent }}</td>
                        <td style="text-align: center">@if ($alquiler->dateReturn != null) {{ $alquiler->dateRent }}
                                                        @else Alquilada desde @php Jenssegers\Date\Date::setLocale('es'); $date = new Jenssegers\Date\Date($alquiler->dateRent); @endphp
                                                        {{ $date->ago() }}
                                                        @endif</td>
                        <td style="text-align: center">{{ $alquiler->usuario->name }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
</div>
@stop